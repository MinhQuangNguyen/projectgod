<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCategoriesAddImageSort extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('sort')->after('image')->default(0);
            $table->text('icon_hover')->after('image')->nullable()->comment("icon sẽ hiển thị khi hover vào dự án ngoài trang chủ");
            $table->integer('bottom_hover_px')->after('icon_hover')->default(0)->comment("số px thay đổi của button khi hover. Dành cho hiệu ứng hiển thị ở ngoài trang chủ");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('sort');
            $table->dropColumn('icon_hover');
            $table->dropColumn('bottom_hover_px');
        });
    }
}

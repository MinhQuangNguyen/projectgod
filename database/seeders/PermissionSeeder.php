<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Domains\Auth\Models\Permission;
use App\Domains\Auth\Models\User;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = Permission::find(1);

        $users->children()->saveMany([
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.banner.list',
                'description' => 'View Banners',
                'sort' => 7,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.introduce.list',
                'description' => 'View Introduces',
                'sort' => 8,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.field.list',
                'description' => 'View Fields',
                'sort' => 9,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.project.list',
                'description' => 'View Projects',
                'sort' => 10,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.category.list',
                'description' => 'View Categories',
                'sort' => 11,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.post.list',
                'description' => 'View Posts',
                'sort' => 12,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.recruitment.list',
                'description' => 'View Recruitments',
                'sort' => 13,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.contact.list',
                'description' => 'View Contacts',
                'sort' => 14,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.setting.list',
                'description' => 'View Settings',
                'sort' => 15,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.menu.list',
                'description' => 'View Menus',
                'sort' => 16,
            ]),
        ]);
    }
}

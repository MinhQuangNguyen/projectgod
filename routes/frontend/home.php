<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\NewsController;
use Tabuna\Breadcrumbs\Trail;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('/', [HomeController::class, 'index'])
    ->name('index')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('frontend.index'));
    });

Route::get('/404', function () { abort(404);});


Route::get('/du-an', [NewsController::class, 'index']);
Route::get('/project', [NewsController::class, 'index']);
// Route::get('/tin-tuc', function () {
//     return redirect('/tin-tuc');
// });
// Route::get('/news', function () {
//     return redirect('/news');
// });


Route::get('/du-an/{slug}.html', [NewsController::class, 'detail']);
Route::get('/project/{slug}.html', [NewsController::class, 'detail']);


Route::get('terms', [TermsController::class, 'index'])
    ->name('pages.terms')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('frontend.index')
            ->push(__('Terms & Conditions'), route('frontend.pages.terms'));
    });

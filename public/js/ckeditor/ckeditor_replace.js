//  setting table width default 100%
const urlBackend = '/js';
const urlCss = '/css';

CKEDITOR.on("dialogDefinition", function (ev) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if (dialogName == 'table') {
        console.log('table');
        var info = dialogDefinition.getContents('info');

        info.get('txtWidth')['default'] = '100%'; // Set default width to 100%
    }
});

const ele = ['content', 'content_en', 'content_p', 'content_p_en', 'description_short', 'description_short_en'];
CKEDITOR.config.contentsCss = [urlCss + '/frontend.css', CKEDITOR.basePath + 'contents.css'];
for (let item of ele) {
    if ($(`#${item}`).length) {
        CKEDITOR.replace(item, {
            filebrowserBrowseUrl: urlBackend + '/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: urlBackend + '/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: urlBackend + '/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl: urlBackend + '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: urlBackend + '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: urlBackend + '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
            extraAllowedContent: 'a span',
        });
        clearInterval(checkExist);
    }
}

const eleClass = ['content-images'];
var checkExist = setInterval(function () {
    for (let item of eleClass) {
        if ($(`.${item}`).length) {
            CKEDITOR.replaceAll(item, {
                filebrowserBrowseUrl: urlBackend + '/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: urlBackend + '/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: urlBackend + '/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: urlBackend + '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: urlBackend + '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: urlBackend + '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                extraAllowedContent: 'a span',
            });
            clearInterval(checkExist);
        }
    }
}, 100);

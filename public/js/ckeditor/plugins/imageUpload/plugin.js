(function () {
    CKEDITOR.plugins.add('imageUpload', {
        init: function (editor) {
            editor.ui.addButton('Chọn nhiều ảnh', {
                label: 'Chọn nhiều ảnh',
                command: 'openDialog',
                toolbar: 'insert,1',
                icon: this.path + 'images/icon.png'
            });

            editor.addCommand('openDialog', {
                exec: function (editor) {
                    var element = CKEDITOR.dom.element.createFromHtml(content);
					CKFinder.popup({
                        chooseFiles: true,
                        width: 800,
                        height: 600,
                        onInit: function (finder) {
                            finder.on('files:choose', function (evt) {
                                evt.data.files.each(file => {
                                    let content = `<img src="${file.getUrl()}" />`
                                    const element = CKEDITOR.dom.element.createFromHtml(content);
                                    editor.insertElement(element)
                                })
                            });
                        }
                    });
                }
            })
        }
    });
})();

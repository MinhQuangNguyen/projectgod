<section class="footer" id="contact" data-scroll-section="">
   <div class="line-wrapper">
     <div class="container">
       <div class="lines">
         <div class="single-line five first"></div>
         <div class="single-line two"></div>
         <div class="single-line last-line five"></div>
       </div>
     </div>
   </div>
   <div class="container">
      <div class="row">
         <div class="col-xl-5 col-lg-4 col-md-6">
            <div class="office">
              <a href="/">
                <img src="/img/logo.png" alt="">
              </a>
              {!! $setting['footter-intro'] !!}
            </div>
         </div>
         <div class="col-xl-2 col-lg-2 col-md-6">
            <div class="hq">
               <h2>Trụ sở</h2>
               {!! $setting['address'] !!}
            </div>
         </div>
         <div class="col-xl-5 col-lg-6 col-md-12 rel">
            <div class="menu">
               <div class="menu-wrap">
                  {!! $setting['social'] !!}
               </div>
            </div>
         </div>
      </div>
   </div>
 </section>
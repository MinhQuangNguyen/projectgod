<div id="showmenu">
    <div class="nav-icon">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div class="mobile-menu">
    <div class="glow"></div>
    <div class="logo">
        <div class="container">
            <h1 title="">
                <a href="" title="">
                    <img src="/img/logo.png" alt="" width="375" height="375" class="lazyloaded" data-ll-status="loaded">
                </a>
            </h1>
        </div>
    </div>
    <div class="line-wrapper">
        <div class="container">
            <div class="lines">
                <div class="single-line five first first-intro"></div>
                <div class="single-line two"></div>
                <div class="single-line last-line five"></div>
            </div>
        </div>
    </div>
    <div class="mobile-menu-wrapper" data-method="imagepicker">
        <div class="menu-col">
            <ul class="main-menu">
                <li><a class="active" href="/">Trang chủ</a> <a class="hover" href="/">Trang chủ</a></li>
                <li>
                    <a class="active" data-scroll-to="" href="/#services">Dịch vụ</a>
                    <a class="hover" data-scroll-to="" href="/#services">Dịch vụ</a></li>
                <li><a class="active" data-scroll-to="" href="/du-an">Dự án</a> <a class="hover" data-scroll-to=""
                        href="/du-an">Dự án</a></li>
                <li><a class="active" data-scroll-to="" href="#contact">Liên hệ</a> <a class="hover" data-scroll-to=""
                        href="#contact">Liên hệ</a></li>
            </ul>
        </div>
        <div class="image-col">
            <div class="mobile-image sculpture active-image" data-id="1">
                <div class="float-transition">
                    <div class="float-image">
                        <img src="{{ asset('/img/than-zeus.png') }}" alt="" width="321" height="1321" class="lazyloaded"
                            data-ll-status="loaded">
                    </div>
                </div>
            </div>
            <div class="mobile-image dog" data-id="2">
                <div class="float-transition">
                    <div class="float-image">
                        <img src="{{ asset('/img/than-bien.png') }}" alt="" width="496" height="1006" class="lazyloaded"
                            data-ll-status="loaded">
                    </div>
                </div>
            </div>
            <div class="mobile-image women" data-id="3">
                <div class="float-transition">
                    <div class="float-image" style="width:280px">
                        <img src="{{ asset('/img/than-chet.png') }}" alt="" width="316" height="1001" class="lazyloaded"
                            data-ll-status="loaded">
                    </div>
                </div>
            </div>
            <div class="mobile-image wall" data-id="4">
                <div class="float-transition">
                    <div class="float-image">
                        <img src="{{ asset('/img/than-chien-tranh.png') }}" alt="" width="520" height="679"
                            class="lazyloaded" data-ll-status="loaded">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

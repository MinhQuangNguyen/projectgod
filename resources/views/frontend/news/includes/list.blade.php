
<section class="projects-subpage" data-scroll-section=""
    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); visibility: visible;">
    <div class="logo">
        <div class="container">
            <h1 title="">
                <a href="/" title="">
                    <img src="/img/logo.png" alt="" width="210" height="210" />
                </a>
            </h1>
        </div>
    </div>

    <div class="line-wrapper">
        <div class="container">
          <div class="lines">
            <div class="single-line five first first-intro"></div>
            <div class="single-line two"></div>
            <div class="single-line last-line five"></div>
          </div>
        </div>
      </div>

      
    <div class="d-none d-lg-block">
        <div class="slider-center">
            <div class="swiper-container swiper-container-coverflow swiper-container-3d swiper-container-initialized swiper-container-horizontal"
                data-method="projectslider">
                <div class="swiper-wrapper" id="swiper-wrapper-87231d1e4fb2c93d" aria-live="polite"
                    style="transition: all 0ms ease 0s; transform: translate3d(-920.667px, 0px, 0px);">
    
                    @foreach($posts as $post)
                    <div class="swiper-slide swiper-slide-visible swiper-slide-active" data-swiper-slide-index="0"
                        role="group" aria-label="4 / 17"
                        style="width: 445.333px; transition: all 0ms ease 0s; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) scale(1); z-index: 1; margin-right: 15px;">
                        <a class="project-showcase cf" href="/du-an/{{$post->slug.'.html' }}">
                            <div class="slide-image left-content">
                                <img width="900" height="439" src="{{ asset($post->image) }}" alt=""
                                    sizes="(max-width: 900px) 100vw, 900px" class="lazyloaded" data-ll-status="loaded">
                            </div>
                            <div class="project-title-big">{{ $post->title }}</div>
                            <div class="project-title left-content">
                                <p>{{ $post->title }}</p>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
        </div>
        <div class="pagination cf">
            <div class="container">
                <div class="right-arrow" tabindex="0" role="button" aria-label="Next slide"
                    aria-controls="swiper-wrapper-87231d1e4fb2c93d"><a href="">Next</a>
                </div>
                <div class="left-arrow" tabindex="0" role="button" aria-label="Previous slide"
                    aria-controls="swiper-wrapper-87231d1e4fb2c93d"><a href="">Prev</a>
                </div>
            </div>
        </div>
        <div class="back-to cf">
            <div class="container">
                <div class="back-btn"><a href="/" title="">Trở về trang chủ</a></div>
            </div>
        </div>
    </div>

    <div class="d-block d-lg-none">
       <div class="container">
            @foreach($posts as $post)
                <div class="item-project-mqn">
                    <a href="/du-an/{{$post->slug.'.html' }}">
                        <img width="900" height="439" src="{{ asset($post->image) }}" alt=""
                            sizes="(max-width: 900px) 100vw, 900px" class="lazyloaded" data-ll-status="loaded">
                        <div class="title-project-mqn">
                            <p>{{ $post->title }}</p>
                        </div>
                    </a>
                </div>
            @endforeach
       </div>
    </div>
</section>

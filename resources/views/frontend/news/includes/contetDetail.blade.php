<section class="showcase" data-scroll-section=""
    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); visibility: visible;">

    @include('frontend.home.includes.logo')

    <div class="container">
        {!! $post->content !!}
        <div class="next-project center">
            <a href="/du-an/{{$news_relates[$random_post]->slug.'.html' }}" class="btn TEST">
                <div class="btn-wrapper">
                    <div class="button active">Dự án kế tiếp</div>
                    <div class="button hover">Dự án kế tiếp</div>
                </div>
            </a>
            @if(isset($news_relates))
            <a href="/du-an/{{$news_relates[$random_post]->slug.'.html' }}" class="next-project-name">
                {{ $news_relates[$random_post]->title }}
            </a>

            @endif
        </div>
    </div>
</section>

@include('frontend.home.includes.team')

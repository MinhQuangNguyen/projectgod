<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="author" content="@yield('meta_author', 'Minh Quang Nguyen')">

    <meta property="og:locale" content="{{ htmlLang() }}" />
    <meta property="og:title" content="@yield('title', appName())" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ request()->fullUrl() }}" />
    <meta property="og:description" name="description"
        content="Thiết kế bộ nhận diện thương hiệu. Phát triển các ứng dụng theo xu hướng mới" />
    <meta property="og:site_name" content="{{ appURL() }}" />
    <meta property="og:image" content="/img/meta.png" />
    <link rel="icon" sizes="94x94" type="image/gif" href="/img/flaticon.png" />
    @yield('meta')

    @stack('before-styles')
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')
    <script src='{{ asset('js/jquery.min.js') }}'></script>
</head>
<style>
    body:not(.loaded) {
      overflow: hidden;
    }
  </style>
<body class="home page-template page-template-tpl-home page-template-tpl-home-php page page-id-5 single-author singular loaded"
    style="">

    <div id="app">
        <div id="preloader" class="preloader preloader_unvisible"
            style="transition: opacity 0.6s ease 0s; background-color: rgb(26, 26, 26);">
            <div class="logo-loader">
                <svg xmlns="http://www.w3.org/2000/svg" style="margin-top:-10px; margin-left:-10px" width="90.26" height="81.538" viewBox="0 0 90.26 81.538">
                    <g id="valknut" transform="translate(-99.816 -151.458)">
                      <path id="Path_8112" data-name="Path 8112" d="M246.57,238.686c-2.5-5.532-2.5-5.686-6.5-11.217-3.962,5.526-3.964,5.674-6.449,11.215C240.407,239.367,240.535,239.292,246.57,238.686Zm0,0" transform="translate(-95.145 -54.047)" fill="#fff"/>
                      <path id="Path_8113" data-name="Path 8113" d="M158.937,177.374a44.888,44.888,0,0,0-52.28-24.368l-6.841,1.832,1.832,6.842a44.946,44.946,0,0,0,17.284,24.914l1.764,1.257,8.22-11.536-1.765-1.259a30.669,30.669,0,0,1-8.622-9.419,30.634,30.634,0,0,1,16.357,4.909,44.572,44.572,0,0,0-7.871,30.185l4.314-.424a40.244,40.244,0,0,1,8.336-28.931l1.363-1.745-1.774-1.325a34.979,34.979,0,0,0-24.42-6.834l-3.009.3,1.248,2.754a35.1,35.1,0,0,0,9.837,12.75l-3.194,4.484a40.609,40.609,0,0,1-13.881-21.2l-.711-2.655,2.655-.711a40.542,40.542,0,0,1,48.05,23.905l.828,2.057,2.037-.876a34.887,34.887,0,0,0,18.133-17.752l1.24-2.752-3-.295a35.089,35.089,0,0,0-15.887,2.113l-2.274-5.015a40.633,40.633,0,0,1,25.211-1.385l2.655.711-.711,2.655a40.569,40.569,0,0,1-44.715,29.654l-2.191-.306-.261,2.2a34.966,34.966,0,0,0,6.293,24.57l1.762,2.457,1.761-2.458a35.1,35.1,0,0,0,6.123-14.9l5.481.525a40.613,40.613,0,0,1-11.42,22.623l-1.944,1.944L143,224.924a40.38,40.38,0,0,1-10.765-19.185l-4.216,1a44.708,44.708,0,0,0,11.916,21.245L144.946,233l5.008-5.008a44.942,44.942,0,0,0,12.935-27.426l.207-2.158-14.1-1.35-.207,2.158a30.655,30.655,0,0,1-3.847,12.178,30.638,30.638,0,0,1-3.928-16.627,44.921,44.921,0,0,0,47.23-33.083l1.833-6.842-6.841-1.832a44.948,44.948,0,0,0-30.121,2.465l-1.974.9,5.85,12.9,1.974-.9a30.662,30.662,0,0,1,12.415-2.727,30.547,30.547,0,0,1-12.442,11.729Zm0,0" transform="translate(0 0)" fill="#fff"/>
                    </g>
                  </svg>                                                
            </div>
            <canvas class="preloader__progress-bar" id="progress-bar" width="300" height="300"
                style="opacity: 1;"></canvas>
            <div class="preloader__progress-percentage" id="progress-percentage"
                style="color: rgb(137, 185, 45); opacity: 0.5;">100</div>
        </div>
        <div class="mouse">
            <div class="cursor" style="top: 521px; left: 945px;"></div>
            <div class="cursor-follower" style="top: 395px; left: 859px;">
                <span class="next">Next</span>
                <span class="prev">Prev</span>
                <span class="view">View Project</span>
            </div>

        </div>
        <div data-barba="wrapper" aria-live="polite">
            <div id="page-wrapper">
               
                {{-- @include('includes.partials.messages') --}}

                <main id="barba-wrapper" data-barba="container" data-barba-namespace="home">
                    <div id="js-scroll">
                        @include('frontend.includes.header', ['menu' => $menu])

                        @yield('content')
                        @include('frontend.includes.footer')
                    </div>
                </main>

                <!-- Messenger Plugin chat Code -->
                <div id="fb-root"></div>
                <script>
                window.fbAsyncInit = function() {
                    FB.init({
                    xfbml            : true,
                    version          : 'v10.0'
                    });
                };

                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                </script>

                <!-- Your Plugin chat code -->
                <div class="fb-customerchat"
                attribution="biz_inbox"
                page_id="109183271278324">
                </div>
            </div>
        </div>
    </div>
    <!--app-->

    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/frontend.js') }}"></script>


    <script src='{{ asset('js/lazyload.min.js') }}'></script>
    <script src='{{ asset('js/main.js') }}'></script>
    <livewire:scripts />
    @include('frontend.includes.js')
    @stack('after-scripts')
</body>

</html>

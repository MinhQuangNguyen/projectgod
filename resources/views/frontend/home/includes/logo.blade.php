<div class="glow"></div>
    <div class="logo">
        <div class="container">
            <h1 title="">
                <a href="/" title="">
                    <img src="/img/logo.png" alt="" width="210" height="210" />
                </a>
            </h1>
        </div>
    </div>
    <div class="line-wrapper">
        <div class="container">
            <div class="lines">
                <div class="single-line five first first-intro"></div>
                <div class="single-line two"></div>
                <div class="single-line last-line five"></div>
            </div>
        </div>
    </div>
    
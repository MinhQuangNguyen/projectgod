
 <section class="intro" data-scroll-section="">
    @include('frontend.home.includes.logo')

    <div class="line-wrapper">
      <div class="container">
        <div class="lines">
          <div class="single-line five first first-intro"></div>
          <div class="single-line two"></div>
          <div class="single-line last-line five"></div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-xl-5 col-md-6">
          <div class="intro-wrapper">
            <div class="title">
                <div class="reveal text-anim-bottom is-inview animated" data-delay="1400"
                data-delay-mobile="1000" data-scroll="" data-scroll-call="reveal">
                <h2><strong>Studio</strong><br> Media Iconic</h2>
            </div>
              <div class="linee reveal fade-line is-inview animated" data-scroll="" data-delay="1800"
                data-delay-mobile="700" data-scroll-call="reveal"></div>
            </div>
            <div class="intro-text reveal fade-anim-top is-inview animated" data-scroll="" data-delay="2200"
            data-delay-mobile="1200" data-scroll-call="reveal" data-scroll-offset="-500">
            {!! $setting['intro'] !!}
        </div>
        <a href="/du-an" class="btn reveal fade-anim-top is-inview animated"
            data-scroll="" data-delay="2400" data-scroll-call="reveal" data-scroll-offset="-500">
            <div class="btn-wrapper">
                <div class="button active">Xem dự án</div>
                <div class="button hover">Xem dự án</div>
            </div>
        </a>
          </div>
        </div>
        <div class="col-xl-6 offset-xl-1 col-md-6">
            <div class="intro-image">
                <div class="position reveal fade-anim-default is-inview animated" data-scroll="" data-delay="900"
                    data-delay-mobile="400" data-scroll-call="reveal">
                    <div class="decoration-text is-inview animated" data-scroll="" data-scroll-speed="-2"
                        data-scroll-delay="0.10"
                        style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 22.2, 0, 1);">
                        <span>Web<br>Studio</span>
                    </div>
                </div>
                <div class="floating-image">
                    <div class="reveal fade-anim-zoom-right is-inview animated" data-delay="700"
                        data-delay-mobile="200" data-scroll="" data-scroll-call="reveal">
                        <img src="{{ asset('/img/than-zeus.png') }}" alt="" width="321" height="1321" class="lazyloaded"
                            data-ll-status="loaded">
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>

<section class="team" data-scroll-section="">
    <div class="line-wrapper">
      <div class="container">
        <div class="lines">
          <div class="single-line five first"></div>
          <div class="single-line two"></div>
          <div class="single-line last-line five"></div>
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xl-5 col-md-6 text-col">
                {!! $setting['trachnhiem'] !!}
            </div>
            <div class="col-xl-5 col-md-6 offset-xl-2 image-content">
                <div class="team-image">
                    <div class="floating-image">
                        <div class="reveal fade-anim-zoom-right is-inview animated" data-delay="200"
                            data-delay-mobile="200" data-scroll="" data-scroll-call="reveal">
                            <img src="{{ asset('/img/than-chien-tranh.png') }}" alt="" width="520" height="679"
                                class="lazyloaded" data-ll-status="loaded">
                        </div>
                    </div>
                    <div class="random">
                        <p>chất lượng <span>uy tín</span></p>
                    </div>
                    <div class="position reveal fade-anim-default is-inview animated" data-scroll="" data-delay="1000"
                        data-delay-mobile="400" data-scroll-call="reveal">
                        <div class="decoration-text is-inview animated" data-scroll="" data-scroll-speed="2"
                            data-scroll-delay="0.10"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                            <span>New<br>Brand</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
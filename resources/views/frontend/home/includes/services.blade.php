<section class="services" id="services" data-scroll-section="">
    <div class="line-wrapper">
      <div class="container">
        <div class="lines">
          <div class="single-line five first"></div>
          <div class="single-line two"></div>
          <div class="single-line last-line five"></div>
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xl-5 col-md-6 text-col">
                <div class="services-wrapper">
                    {!! $setting['service'] !!}
                    <a href="#contact"
                        class="btn white-line reveal fade-anim-left-btn is-inview animated" data-scroll-to=""
                        data-scroll="" data-delay="400" data-delay-mobile="200" data-scroll-call="reveal">
                        <div class="btn-wrapper">
                            <div class="button active">Liên hệ với tôi</div>
                            <div class="button hover">Liên hệ với tôi</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 offset-xl-1 image-content">
                <div class="services-image">
                    <div class="floating-image">
                        <div class="reveal fade-anim-zoom-right is-inview animated" data-delay="200"
                            data-delay-mobile="200" data-scroll="" data-scroll-call="reveal">
                            <img src="{{ asset('/img/than-bien.png') }}" alt="" width="496" height="1006" class="lazyloaded"
                                data-ll-status="loaded">
                        </div>
                    </div>
                    <div class="random black">
                        <p>Tân tâm <span>dự án</span></p>
                    </div>
                    <div class="position reveal fade-anim-default is-inview animated" data-scroll="" data-delay="1000"
                        data-delay-mobile="400" data-scroll-call="reveal">
                        <div class="decoration-text is-inview animated" data-scroll="" data-scroll-speed="2"
                            data-scroll-delay="0.10"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                            <span>our<br>services </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
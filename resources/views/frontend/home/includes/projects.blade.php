<section class="projects" data-scroll-section="">
    <div class="line-wrapper">
        <div class="container">
            <div class="lines">
                <div class="single-line five first"></div>
                <div class="single-line two"></div>
                <div class="single-line last-line five"></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-4 image-content">
                <div class="projects-image">
                    <div class="floating-image">
                        <div class="reveal fade-anim-zoom-right is-inview animated" data-delay="200"
                            data-delay-mobile="200" data-scroll="" data-scroll-call="reveal">
                            <img src="{{ asset('/img/than-chet.png') }}" alt="" width="316" height="1001" class="lazyloaded"
                                data-ll-status="loaded">
                        </div>
                    </div>
                    <div class="random">
                        <p>Cập nhật <span>xu hướng</span></p>
                    </div>
                    <div class="position reveal fade-anim-default is-inview animated" data-scroll="" data-delay="1000"
                        data-delay-mobile="400" data-scroll-call="reveal">
                        <div class="decoration-text is-inview animated" data-scroll="" data-scroll-speed="2"
                            data-scroll-delay="0.10"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                            <span>our<br>projects </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-8 text-col">
                <div class="projects-wrapper cf">
                    @foreach($posts as $post)
                        <a href="/du-an/{{$post->slug.'.html' }}" class="project-link ">
                            <div class="project">
                                <div class="reveal fade-anim-bottom is-inview animated" data-delay="500"
                                    data-delay-mobile="200" data-scroll="" data-scroll-call="reveal">
                                    <div class="project-image-wrap">
                                        <div class="img-hover">
                                            <picture class="attachment-project size-project wp-post-image">
                                                <img width="725" height="345" src="{{ asset($post->image) }}" alt="Zenn&#39;s Foto"
                                                    class="lazyloaded" data-ll-status="loaded">
                                            </picture>
                                        </div>
                                    </div>
                                </div>
                                <div class="project-name reveal text-anim-top is-inview animated" data-scroll=""
                                    data-delay="700" data-delay-mobile="400" data-scroll-call="reveal">
                                    <h2>{{ $post->title }}</h2>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="project-btn">
                    <a href="/du-an" class="btn reveal fade-anim-left-btn is-inview animated" data-scroll=""
                        data-delay="1400" data-scroll-call="reveal">
                        <div class="btn-wrapper">
                            <div class="button active">Xem thêm</div>
                            <div class="button hover">Xem thêm</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


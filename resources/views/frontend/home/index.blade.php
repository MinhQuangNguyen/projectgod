@extends('frontend.layouts.iconic')

@section('title', __('Home'))

@section('content')
@include('frontend.home.includes.intro')
@include('frontend.home.includes.services')
@include('frontend.home.includes.projects')
@include('frontend.home.includes.team')
@endsection

@if (count($paginator))
<div class="pagination-main">
    <div class="row justify-content-center">
        @if ($paginator->onFirstPage())
        <div class="btn-kc btn-back partner-prev disabled">
            <div class="btn-content"></div>
        </div>
            @else
                <div class="btn-kc btn-back partner-prev">
                    <a class="btn-content" href="{{ $paginator->previousPageUrl() }}"></a>
                </div>
            @endif
                <div class="item-number-pagination">
                    <span>{{ convertNumber($paginator->currentPage()) }}/{{ convertNumber($paginator->lastPage()) }}</span>
                </div>
            @if ($paginator->hasMorePages())
                <div class="btn-kc btn-next partner-next">
                    <a class="btn-content" href="{{ $paginator->nextPageUrl() }}"></a>
                </div>
            @else
                <div class="btn-kc btn-next partner-next disabled">
                    <div class="btn-content"></div>
                </div>
            @endif
    </div>
</div>
@endif

@extends('backend.layouts.app')

@section('title', __('Detail Contact'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Detail Category')
        </x-slot>

        <x-slot name="headerActions">
            <x-utils.link class="card-header-action" :href="route('admin.contact.index')" :text="__('Back')" />
        </x-slot>

        <x-slot name="body">
            <div>
                <div class="form-group row">
                    <label for="title" class="col-md-2 col-form-label">@lang('Company')</label>

                    <div class="col-md-10">
                        <input type="text" name="company" class="form-control" placeholder="{{ __('Company') }}" value="{{ old('company') ?? $contact->company }}" readonly />
                    </div>
                </div><!--form-group-->
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                    <div class="col-md-10">
                        <input type="text"  name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $contact->name }}" readonly />
                    </div>
                </div><!--form-group-->

                <div class="form-group row">
                    <label for="email" class="col-md-2 col-form-label">@lang('Email')</label>

                    <div class="col-md-10">
                        <input type="text"  name="email" class="form-control" placeholder="{{ __('Email') }}" value="{{ old('email') ?? $contact->email }}" readonly />
                    </div>
                </div><!--form-group-->

                <div class="form-group row">
                    <label for="phone" class="col-md-2 col-form-label">@lang('Phone')</label>

                    <div class="col-md-10">
                        <input type="text"  name="phone" class="form-control" placeholder="{{ __('Phone') }}" value="{{ old('phone') ?? $contact->phone }}" readonly />
                    </div>
                </div><!--form-group-->

                <div class="form-group row">
                    <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                    <div class="col-md-10">
                        <textarea type="text" name="content" class="form-control" placeholder="{{ __('Content') }}" readonly>{{ old('content') ?? $contact->content }}</textarea>
                    </div>
                </div><!--form-group-->
            </div>
        </x-slot>
    </x-backend.card>
@endsection

@extends('backend.layouts.app')

@section('title', __('Contact Management'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Contact Management')
        </x-slot>

        <x-slot name="body">
            <livewire:backend.contacts-table />
        </x-slot>
    </x-backend.card>
@endsection

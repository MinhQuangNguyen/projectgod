@extends('backend.layouts.app')

@section('title', __('Create Menu'))

@section('content')
    <x-forms.post :action="route('admin.menu.store')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Menu')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.menu.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name_en" class="col-md-2 col-form-label">@lang('Name (En)')</label>

                        <div class="col-md-10">
                            <input type="text" name="name_en" class="form-control" placeholder="{{ __('Name (En)') }}" value="{{ old('name_en') }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="category_id" class="col-md-2 col-form-label">@lang('Category')</label>

                        <div class="col-md-10">
                            <select name="category_id" class="form-control">
                                <option value=""></option>
                                @foreach($categories as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="link" class="col-md-2 col-form-label">@lang('Link')</label>

                        <div class="col-md-10">
                            <input type="text" name="link" class="form-control" placeholder="{{ __('Link') }}" value="{{ old('link') }}" maxlength="255" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="link_en" class="col-md-2 col-form-label">@lang('Link (En)')</label>

                        <div class="col-md-10">
                            <input type="text" name="link_en" class="form-control" placeholder="{{ __('Link (En)') }}" value="{{ old('link_en') }}" maxlength="255" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="parent_id" class="col-md-2 col-form-label">@lang('Parent')</label>

                        <div class="col-md-10">
                            <select name="parent_id" class="form-control">
                                <option value=""></option>
                                @foreach($parents as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="sort" class="col-md-2 col-form-label">@lang('Sort')</label>

                        <div class="col-md-10">
                            <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') }}" required />
                        </div>
                    </div>

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>

    @push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("select").select2({
                placeholder: "{{ __('Select a option') }}",
                allowClear: true
            });
        })
    </script>
    @endpush('after-scripts')
@endsection

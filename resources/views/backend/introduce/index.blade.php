@extends('backend.layouts.app')

@section('title', __('Introduce Management'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Introduce Management')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action"
                    :href="route('admin.introduce.create')"
                    :text="__('Create Introduce')"
                />
            </x-slot>
        @endif

        <x-slot name="body">
            <livewire:backend.introduces-table />
        </x-slot>
    </x-backend.card>
@endsection

@extends('backend.layouts.app')

@section('title', __('Update Introduce'))

@section('content')
    <x-forms.patch :action="route('admin.introduce.update', $introduce)">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Introduce')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.introduce.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text"  name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $introduce->name }}" maxlength="100" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="link" class="col-md-2 col-form-label">@lang('Link')</label>

                        <div class="col-md-10">
                            <input type="text"  name="link" class="form-control" placeholder="{{ __('Link') }}" value="{{ old('link') ?? $introduce->link }}" maxlength="255" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="sort" class="col-md-2 col-form-label">@lang('Sort')</label>

                        <div class="col-md-10">
                            <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') ?? $introduce->sort }}" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                        <div class="col-md-10">
                            <textarea type="text"  name="content" id="content" class="form-control" placeholder="{{ __('Content') }}">{{ old('content') ?? $introduce->content }}</textarea>
                        </div>
                    </div><!--form-group-->

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>

    @push('after-scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('js/ckeditor/ckeditor_replace.js')}}"></script>
    @endpush('after-scripts')
@endsection

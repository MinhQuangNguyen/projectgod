<div class="card">
    <h5 class="card-header">
        <a data-toggle="collapse" href="#collapse-position" aria-expanded="true" aria-controls="collapse-position" id="heading-position" class="d-block">
            <i class="fa fa-chevron-down float-right"></i>
            @lang('Position Project')
        </a>
    </h5>
    <div id="collapse-position" class="collapse show" aria-labelledby="heading-position">
        <div class="card-body">
            <div class="form-group row">
                <label for="title" class="col-md-2 col-form-label">@lang('Title')</label>

                <div class="col-md-10">
                    <input type="text" name="positions[title]" class="form-control" placeholder="{{ __('Title') }}" value="{{ old('positions.title') ?? $project->positions['title'] ?? __('Position Project') }}" maxlength="100" required />
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="title_en" class="col-md-2 col-form-label">@lang('Title (En)')</label>

                <div class="col-md-10">
                    <input type="text" name="positions[title_en]" class="form-control" placeholder="{{ __('Title (En)') }}" value="{{ old('positions.title_en') ?? 'Position' }}" maxlength="100" required />
                </div>
            </div>
            @endif

            <div class="form-group row">
                <label for="description_short" class="col-md-2 col-form-label">@lang('Description Short')</label>

                <div class="col-md-10">
                    <textarea type="text" name="positions[description_short]" id="description_short" class="form-control" placeholder="{{ __('Description Short') }}">{{ old('positions.description_short') ?? (isset($project) && isset($project->positions['description_short']) ? $project->positions['description_short'] : null) }}</textarea>
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="description_short_en" class="col-md-2 col-form-label">@lang('Description Short (En)')</label>

                <div class="col-md-10">
                    <textarea type="text" name="positions[description_short_en]" id="description_short_en" class="form-control" placeholder="{{ __('Description Short (En)') }}">{{ old('positions.description_short_en') }}</textarea>
                </div>
            </div>
            @endif

            <div class="form-group row">
                <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                <div class="col-md-10">
                    <textarea type="text" name="positions[content]" id="content_p" class="form-control" placeholder="{{ __('Content') }}">{{ old('positions.content') ?? (isset($project) && isset($project->positions['content']) ? $project->positions['content'] : null) }}</textarea>
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                <div class="col-md-10">
                    <textarea type="text" name="positions[content_en]" id="content_p_en" class="form-control" placeholder="{{ __('Content (En)') }}">{{ old('positions.content_en') }}</textarea>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@extends('backend.layouts.app')

@section('title', __('Project Management'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Project Management')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action"
                    :href="route('admin.project.create')"
                    :text="__('Create Project')"
                />
            </x-slot>
        @endif

        <x-slot name="body">
            <livewire:backend.projects-table />
        </x-slot>
    </x-backend.card>
@endsection

@inject('model', '\App\Models\Category')

@extends('backend.layouts.app')

@section('title', __('Update Setting'))

@section('content')
    <x-forms.patch :action="route('admin.setting.update', $setting)">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Setting')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.setting.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text"  name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $setting->name }}" maxlength="255" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="value" class="col-md-2 col-form-label">@lang('Value')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="value" class="form-control" id="content" placeholder="{{ __('Value') }}" maxlength="255" required>{{ old('value') ?? $setting->value }}</textarea>
                        </div>
                    </div><!--form-group-->
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>

    @push('after-scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('js/ckeditor/ckeditor_replace.js')}}"></script>
    @endpush('after-scripts')
@endsection

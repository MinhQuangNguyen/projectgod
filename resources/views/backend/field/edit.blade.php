@inject('model', '\App\Models\Category')

@extends('backend.layouts.app')

@section('title', __('Update Field'))

@section('content')
    <x-forms.patch :action="route('admin.field.update', $field)" enctype="multipart/form-data">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Field')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.field.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                                <div class="col-md-10">
                                    <input type="text"  name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $field->name }}" maxlength="100" required />
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                                <label for="description" class="col-md-2 col-form-label">@lang('Description')</label>

                                <div class="col-md-10">
                                    <textarea type="text" name="description" class="form-control" placeholder="{{ __('Description') }}">{{ old('description') ?? $field->description }}</textarea>
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                                <label for="slug" class="col-md-2 col-form-label">@lang('Slug')</label>

                                <div class="col-md-10">
                                    <input type="text"  name="slug" class="form-control" placeholder="{{ __('Slug') }}" value="{{ old('slug') ?? $field->slug }}" maxlength="255" required />
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                                <label for="banner" class="col-md-2 col-form-label">@lang('Banner')</label>

                                <div class="col-md-10">
                                    <div class="custom-file">
                                        <input type="file" name="banner" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">{{ $field->banner ? $field->banner : __('Choose file') }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sort" class="col-md-2 col-form-label">@lang('Sort')</label>

                                <div class="col-md-10">
                                    <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') ?? $field->sort }}" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sort" class="col-md-2 col-form-label">@lang('Active')</label>

                                <div class="col-md-10" style="line-height: 32px;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="active" id="active1" value="{{ $model::ACTIVE }}" {{ $field->active === $model::ACTIVE ? 'checked' : '' }}>
                                        <label class="form-check-label" for="active1">@lang('Active')</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="active" id="active0" value="{{ $model::INACTIVE }}" {{ $field->active === $model::INACTIVE ? 'checked' : '' }}>
                                        <label class="form-check-label" for="active0">@lang('Inactive')</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="text-center active text-muted form-control" style="height: auto;">
                                        <img class="cursor img-thumbnail"
                                            id="images" height="300" width="300"
                                            src="{{ $field->image ? $field->image : url('img/no_image.png') }}"
                                            onclick="selectImg('images', 'image-pre')"
                                        />
                                        <input type="hidden" name="image" id="image-pre" value="{{ $field->image }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>

    @push('after-scripts')
    <script type="text/javascript">
        function selectImg(imgId, inputId) {
            let trash = imgId.substring(3, 4);
            CKFinder.modal({
                height: 550,
                chooseFiles: true,
                onInit: function(finder) {
                    finder.on('files:choose', function(evt) {
                        var file = evt.data.files.first();
                        document.getElementById(imgId).src = file.getUrl();
                        document.getElementById(inputId).value = file.getUrl();
                        $("#trash"+trash).show();
                    });
                    finder.on('file:choose:resizedImage', function(evt) {
                        document.getElementById(imgId).src = evt.data.resizedUrl;
                        document.getElementById(inputId).value = evt.data.resizedUrl;
                        $("#trash"+trash).show();
                    });
                }
            });
        }

        $(document).ready(function() {
            $('input[name="banner"]').change(function(e){
                var fileName = e.target.files[0].name;
                $('.custom-file-label').html(fileName);
            });
        });
    </script>
    @endpush('after-scripts')
@endsection

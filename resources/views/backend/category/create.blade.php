@inject('model', '\App\Models\Category')

@extends('backend.layouts.app')

@section('title', __('Create Category'))

@section('content')
    <x-forms.post :action="route('admin.category.store')" enctype="multipart/form-data">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Category')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.category.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div x-data="{type : '{{ $model::TYPE_NEW }}'}">
                    <div class="form-group row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                                <div class="col-md-10">
                                    <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') }}" maxlength="100" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name_en" class="col-md-2 col-form-label">@lang('Name (En)')</label>

                                <div class="col-md-10">
                                    <input type="text" name="name_en" class="form-control" placeholder="{{ __('Name (En)') }}" value="{{ old('name_en') }}" maxlength="100" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-2 col-form-label">@lang('Description')</label>

                                <div class="col-md-10">
                                    <textarea type="text" name="description" class="form-control" placeholder="{{ __('Description') }}">{{ old('description') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description_en" class="col-md-2 col-form-label">@lang('Description (En)')</label>

                                <div class="col-md-10">
                                    <textarea type="text" name="description_en" class="form-control" placeholder="{{ __('Description (En)') }}">{{ old('description_en') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slug" class="col-md-2 col-form-label">@lang('Slug')</label>

                                <div class="col-md-10">
                                    <input type="text" name="slug" class="form-control" placeholder="{{ __('Slug') }}" value="{{ old('slug') }}" maxlength="255" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slug_en" class="col-md-2 col-form-label">@lang('Slug (En)')</label>

                                <div class="col-md-10">
                                    <input type="text" name="slug_en" class="form-control" placeholder="{{ __('Slug (En)') }}" value="{{ old('slug_en') }}" maxlength="255" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label">@lang('Type')</label>

                                <div class="col-md-10">
                                    <select name="type" class="form-control" required x-on:change="type = $event.target.value">
                                        <option value="{{ $model::TYPE_NEW }}">@lang('New')</option>
                                        <option value="{{ $model::TYPE_LIBRARY }}">@lang('Library')</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sort" class="col-md-2 col-form-label">@lang('Sort')</label>

                                <div class="col-md-10">
                                    <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') }}" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="active" class="col-md-2 col-form-label">@lang('Status')</label>

                                <div class="col-md-10" style="line-height: 32px;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="active" id="active1" value="{{ $model::ACTIVE }}" checked>
                                        <label class="form-check-label" for="active1">@lang('Active')</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="active" id="active0" value="{{ $model::INACTIVE }}">
                                        <label class="form-check-label" for="active0">@lang('Inactive')</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="text-center active text-muted form-control" style="height: auto;">
                                        <img class="cursor img-thumbnail"
                                            id="images" height="300" width="300"
                                            src="{{ url('img/no_image.png') }}"
                                            onclick="selectImg('images', 'image-pre')"
                                        />
                                        <input type="hidden" name="image" id="image-pre" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>

    @push('after-scripts')
    <script type="text/javascript">
        function selectImg(imgId, inputId) {
            let trash = imgId.substring(3, 4);
            CKFinder.modal({
                height: 550,
                chooseFiles: true,
                onInit: function(finder) {
                    finder.on('files:choose', function(evt) {
                        var file = evt.data.files.first();
                        document.getElementById(imgId).src = file.getUrl();
                        document.getElementById(inputId).value = file.getUrl();
                        $("#trash"+trash).show();
                    });
                    finder.on('file:choose:resizedImage', function(evt) {
                        document.getElementById(imgId).src = evt.data.resizedUrl;
                        document.getElementById(inputId).value = evt.data.resizedUrl;
                        $("#trash"+trash).show();
                    });
                }
            });
        }
    </script>
    @endpush('after-scripts')
@endsection

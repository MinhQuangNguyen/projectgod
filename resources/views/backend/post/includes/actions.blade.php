<x-utils.edit-button :href="route('admin.post.edit', $post)" />
<x-utils.delete-button :href="route('admin.post.destroy', $post)" />

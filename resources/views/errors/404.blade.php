<style>

    body{
        padding:0;
        margin:0;
    }

    .item-backround-404{
        position: relative;
        width: 100%;
        background-repeat: no-repeat;
        margin: 0 auto;
        background-size: cover;
        height: 100%;
        background-position: 0px 50%;
    }

    .title{
         font: normal normal bold 42px/34px Montserrat;
        letter-spacing: 0.95px;
        text-transform: uppercase;
        margin-bottom: 10px;
        display: block;
        padding-top: 5%;
        color: white;
        padding-left: 56px;
        text-align: center;
    }

    .container-fluid{
        padding: 0 !important;
    }

    .btn-back-home-404-mqn{
        position: absolute;
        bottom: 5%;
        background: white;
        padding: 10px 30px;
        border-radius: 50px;
        left: 41%;
    }

    .btn-back-home-404-mqn a{
        color: #152535;
        font: normal normal bold 18px/34px Montserrat;
        text-decoration: none;
        text-transform: uppercase;
    }

@media (min-width: 768px) {

}

@media (min-width: 992px) {

}

@media (min-width: 1024px) {

}

@media (min-width: 1366px) {

}

@media (min-width: 1600px) {
}


</style>

<div class="item-backround-404" style="background-image: url('/img/404.png');">
    <div class="container-fluid">
        <div class="row">
           <div class="col-12">
            <div class="title-404">404</div>
            <div class="title-not-found">page not found</div>
            <div class="btn-back-home-404-mqn">
                <a href="/">Trở về trang chủ</a>
            </div>
        </div>
    </div>
</div>
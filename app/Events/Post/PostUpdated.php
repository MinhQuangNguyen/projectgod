<?php

namespace App\Events\Post;

use App\Models\Post;
use Illuminate\Queue\SerializesModels;

/**
 * Class PostUpdated.
 */
class PostUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $post;

    /**
     * @param $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }
}

<?php

namespace App\Http\Livewire\Backend;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class BannersTable.
 */
class BannersTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'name';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Category::where('type', Category::TYPE_SLIDE)->orderBy('sort', 'asc');
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->searchable(function ($builder, $term) {
                    return $builder->whereTranslationLike('name', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('categories.*')->join('category_translations as t', function ($query) use($locale) {
                        $query->on('categories.id', '=', 't.category_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.name', $direction);
                })
                ->format(function (Category $model) {
                    return view('backend.banner.includes.name', ['banner' => $model]);
                }),
            Column::make(__('Description'), 'description')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('description', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('categories.*')->join('category_translations as t', function ($query) use($locale) {
                        $query->on('categories.id', '=', 't.category_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.description', $direction);
                })
                ->format(function (Category $model) {
                    return view('backend.banner.includes.description', ['banner' => $model]);
                }),
            Column::make(__('Position'), 'key')
                ->sortable()
                ->format(function (Category $model) {
                    $rs = current(array_filter(Category::KEYS, function($item) use ($model) {
                        return $model->key === $item['key'];
                    }));
                    if($rs) return __($rs['value']);

                    return 'N/A';
                }),
            Column::make(__('Status'), 'active')
                ->sortable()
                ->format(function (Category $model) {
                    if ($model->active === Category::ACTIVE) {
                        return __('Active');
                    }

                    if ($model->active === Category::INACTIVE) {
                        return __('Inactive');
                    }

                    return 'N/A';
                }),
            Column::make(__('UpdatedAt'), 'updated_at')
                ->sortable()
                ->format(function (Category $model) {
                    return date('d/m/Y', strtotime($model->updated_at));
                }),
            Column::make(__('Actions'))
                ->format(function (Category $model) {
                    return view('backend.banner.includes.actions', ['banner' => $model]);
                }),
        ];
    }
}

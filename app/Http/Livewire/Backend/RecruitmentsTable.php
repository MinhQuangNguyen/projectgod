<?php

namespace App\Http\Livewire\Backend;

use App\Models\Recruitment;
use App\Models\Degree;
use App\Models\Department;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class RecruitmentsTable.
 */
class RecruitmentsTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'title';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Recruitment::query()->orderBy('sort', 'asc');
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Title'), 'title')
                ->searchable(function ($builder, $term) {
                    return $builder->whereTranslationLike('title', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('recruitments.*')->join('recruitment_translations as t', function ($query) use($locale) {
                        $query->on('recruitments.id', '=', 't.recruitment_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.title', $direction);
                })
                ->format(function (Recruitment $model) {
                    return view('backend.recruitment.includes.title', ['recruitment' => $model]);
                }),
            Column::make(__('Description Short'), 'description')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('description', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('recruitments.*')->join('recruitment_translations as t', function ($query) use($locale) {
                        $query->on('recruitments.id', '=', 't.recruitment_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.description', $direction);
                })
                ->format(function (Recruitment $model) {
                    return view('backend.recruitment.includes.description', ['recruitment' => $model]);
                }),
            Column::make(__('Rank'), 'rank')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('rank', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('recruitments.*')->join('recruitment_translations as t', function ($query) use($locale) {
                        $query->on('recruitments.id', '=', 't.recruitment_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.rank', $direction);
                })
                ->format(function (Recruitment $model) {
                    return view('backend.recruitment.includes.rank', ['recruitment' => $model]);
                }),
            Column::make(__('Form'), 'form')
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('recruitments.*')->join('recruitment_translations as t', function ($query) use($locale) {
                        $query->on('recruitments.id', '=', 't.recruitment_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.form', $direction);
                })
                ->format(function (Recruitment $model) {
                    $dataF = [];
                    foreach($model->form as $itemT) {
                        $rs = current(array_filter(Recruitment::FORMS, function($item) use ($itemT) {
                            return $itemT === $item['key'];
                        }));
                        if($rs) array_push($dataF, __($rs['value']));
                    }

                    return implode(", ", $dataF);
                }),
            Column::make(__('Degree'), 'degree')
                ->format(function (Recruitment $model) {
                    $degree = Degree::find($model->degree_id);
                    return view('backend.recruitment.includes.degree', ['degree' => $degree]);
                }),
            Column::make(__('Department'), 'department')
                ->format(function (Recruitment $model) {
                    $department = Department::find($model->department_id);
                    return view('backend.recruitment.includes.department', ['department' => $department]);
                }),
            Column::make(__('Expiration Date'), 'expired')
                ->sortable()
                ->format(function (Recruitment $model) {
                    return date('d/m/Y', strtotime($model->expired));
                }),
            Column::make(__('Actions'))
                ->format(function (Recruitment $model) {
                    return view('backend.recruitment.includes.actions', ['recruitment' => $model]);
                }),
        ];
    }
}

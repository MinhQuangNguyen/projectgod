<?php

namespace App\Http\Livewire\Backend;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class PostsTable.
 */
class PostsTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'title';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Post::query()->orderBy('sort', 'asc');
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Title'), 'title')
                ->searchable(function ($builder, $term) {
                    return $builder->whereTranslationLike('title', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('posts.*')->join('post_translations as t', function ($query) use($locale) {
                        $query->on('posts.id', '=', 't.post_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.title', $direction);
                })
                ->format(function (Post $model) {
                    return view('backend.post.includes.title', ['post' => $model]);
                }),
            Column::make(__('Description'), 'description_short')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('description_short', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('posts.*')->join('post_translations as t', function ($query) use($locale) {
                        $query->on('posts.id', '=', 't.post_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.description_short', $direction);
                })
                ->format(function (Post $model) {
                    return view('backend.post.includes.description', ['post' => $model]);
                }),
            Column::make(__('Category'), 'category_id')
                ->format(function (Post $model) {
                    $category = Category::find($model->category_id);
                    return view('backend.post.includes.category', ['category' => $category]);
                }),
            Column::make(__('Type'), 'type')
                ->sortable()
                ->format(function (Post $model) {
                    if ($model->type === Post::TYPE_NEW_IMAGE) {
                        return __('New, Image');
                    }

                    if ($model->type === Post::TYPE_VIDEO) {
                        return __('Video');
                    }

                    return 'N/A';
                }),
            Column::make(__('Status'), 'active')
                ->sortable()
                ->format(function (Post $model) {
                    if ($model->active === Post::ACTIVE) {
                        return __('Active');
                    }

                    if ($model->active === Post::INACTIVE) {
                        return __('Inactive');
                    }

                    return 'N/A';
                }),
            Column::make(__('CreatedAt'), 'created_at')
                ->sortable()
                ->format(function (Post $model) {
                    return date('d/m/Y', strtotime($model->created_at));
                }),
            Column::make(__('Actions'))
                ->format(function (Post $model) {
                    return view('backend.post.includes.actions', ['post' => $model]);
                }),
        ];
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Category\StoreCategoryRequest;
use App\Http\Requests\Backend\Category\EditCategoryRequest;
use App\Http\Requests\Backend\Category\UpdateCategoryRequest;
use App\Http\Requests\Backend\Category\DeleteCategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;

/**
 * Class CategoryController.
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * CategoryController constructor.
     *
     * @param  CategoryService  $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.category.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.category.create');
    }

    /**
     * @param  StoreCategoryRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreCategoryRequest $request)
    {
        $data = $request->only('name', 'name_en', 'description', 'image', 'description_en', 'slug', 'slug_en', 'type', 'sort', 'active');
        $this->categoryService->store($data);

        return redirect()->route('admin.category.index')->withFlashSuccess(__('The category was successfully created.'));
    }

    /**
     * @param  EditCategoryRequest  $request
     * @param  Category  $category
     *
     * @return mixed
     */
    public function edit(EditCategoryRequest $request, Category $category)
    {
        return view('backend.category.edit')->withCategory($category);
    }

    /**
     * @param  UpdateCategoryRequest  $request
     * @param  Category  $category
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $data = $request->validated();
        $this->categoryService->update($category, $data);

        return redirect()->route('admin.category.index')->withFlashSuccess(__('The category was successfully updated.'));
    }

    /**
     * @param  DeleteCategoryRequest  $request
     * @param  Category  $category
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteCategoryRequest $request, Category $category)
    {
        $this->categoryService->destroy($category);

        return redirect()->route('admin.category.index')->withFlashSuccess(__('The category was successfully deleted.'));
    }
}

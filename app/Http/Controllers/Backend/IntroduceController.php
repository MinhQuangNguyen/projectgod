<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Menu\StoreMenuRequest;
use App\Http\Requests\Backend\Menu\EditMenuRequest;
use App\Http\Requests\Backend\Menu\UpdateMenuRequest;
use App\Http\Requests\Backend\Menu\DeleteMenuRequest;
use App\Models\Menu;
use App\Services\MenuService;
use App\Http\Controllers\Controller;

/**
 * Class IntroduceController.
 */
class IntroduceController extends Controller
{
    /**
     * @var MenuService
     */
    protected $menuService;

    /**
     * IntroduceController constructor.
     *
     * @param  MenuService  $menuService
     */
    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.introduce.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.introduce.create');
    }

    /**
     * @param  StoreMenuRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreMenuRequest $request)
    {
        $data = $request->only('name', 'name_en', 'link', 'link_en', 'content', 'content_en', 'sort');
        $this->menuService->store($data, 'introduce');

        return redirect()->route('admin.introduce.index')->withFlashSuccess(__('The introduce was successfully created.'));
    }

    /**
     * @param  EditMenuRequest  $request
     * @param  Menu  $introduce
     *
     * @return mixed
     */
    public function edit(EditMenuRequest $request, Menu $introduce)
    {
        return view('backend.introduce.edit')->withIntroduce($introduce);
    }

    /**
     * @param  UpdateMenuRequest  $request
     * @param  Menu  $introduce
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateMenuRequest $request, Menu $introduce)
    {
        $this->menuService->update($introduce, $request->only('name', 'link', 'content', 'sort'), 'introduce');

        return redirect()->route('admin.introduce.index')->withFlashSuccess(__('The introduce was successfully updated.'));
    }

    /**
     * @param  DeleteMenuRequest  $request
     * @param  Menu  $menu
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteMenuRequest $request, Menu $introduce)
    {
        $this->menuService->destroy($introduce, 'introduce');

        return redirect()->route('admin.introduce.index')->withFlashSuccess(__('The introduce was successfully deleted.'));
    }
}

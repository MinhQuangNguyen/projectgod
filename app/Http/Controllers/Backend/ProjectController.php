<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Project\StoreProjectRequest;
use App\Http\Requests\Backend\Project\EditProjectRequest;
use App\Http\Requests\Backend\Project\UpdateProjectRequest;
use App\Http\Requests\Backend\Project\DeleteProjectRequest;
use App\Models\Project;
use App\Services\ProjectService;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ProjectController.
 */
class ProjectController extends Controller
{
    /**
     * @var ProjectService
     */
    protected $projectService;
    const PATH_IMAGE = '/images/project';
    const PATH_POSITION = '/images/positions';
    const PATH_UTILITY = '/images/utilities';
    const PATH_IMAGES = '/images/images';
    const PATH_BANNER = '/images/project/banner';

    /**
     * ProjectController constructor.
     *
     * @param  ProjectService  $projectService
     * @param  CategoryService  $categoryService
     */
    public function __construct(ProjectService $projectService, CategoryService $categoryService)
    {
        $this->projectService = $projectService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.project.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.project.create')->withCategories($this->categoryService->getProjectCategories());
    }

    /**
     * @param  StoreProjectRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $image = $request->file('image');
        if($image) {
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path(self::PATH_IMAGE);
            $image->move($destinationPath, $imageName);
            $data['image'] = self::PATH_IMAGE.'/'.$imageName;
        }

        $banner = $request->file('banner');
        if($banner) {
            $imageBanner = time().'.'.$image->getClientOriginalExtension();
            $destinationPathB = public_path(self::PATH_BANNER);
            $banner->move($destinationPathB, $imageBanner);
            $data['banner'] = self::PATH_BANNER.'/'.$imageBanner;
        }

        $positionImg = $request->file('positions');
        if($positionImg) {
            $positionImg = $positionImg['image'];
            $imageNamePosition = time().'.'.$positionImg->getClientOriginalExtension();
            $destinationPathP = public_path(self::PATH_POSITION);
            $positionImg->move($destinationPathP, $imageNamePosition);
            $data['positions']['image'] = self::PATH_POSITION.'/'.$imageNamePosition;
        }

        $project = $this->projectService->store($data);

        foreach ($request->input('utilities.images', []) as $file) {
            $dstfile = public_path(self::PATH_UTILITY).'/'.$file;
            if (!file_exists(dirname($dstfile))) {
                mkdir(dirname($dstfile), 0777, true);
            }
            copy(storage_path('tmp/uploads/' . $file), $dstfile);
            $project->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('utilities');
        }

        foreach ($request->input('images.images', []) as $file) {
            $dstfile1 = public_path(self::PATH_IMAGES).'/'.$file;
            if (!file_exists(dirname($dstfile1))) {
                mkdir(dirname($dstfile1), 0777, true);
            }
            copy(storage_path('tmp/uploads/' . $file), $dstfile1);
            $project->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('images');
        }

        return redirect()->route('admin.project.index')->withFlashSuccess(__('The project was successfully created.'));
    }

    /**
     * @param  EditProjectRequest  $request
     * @param  Project  $project
     *
     * @return mixed
     */
    public function edit(EditProjectRequest $request, Project $project)
    {
        return view('backend.project.edit')
        ->withCategories($this->categoryService->getProjectCategories())
        ->withProject($project);
    }

    /**
     * @param  UpdateProjectRequest  $request
     * @param  Project  $project
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        $data = $request->all();
        $image = $request->file('image');
        if($image) {
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path(self::PATH_IMAGE);
            $image->move($destinationPath, $imageName);
            $data['image'] = self::PATH_IMAGE.'/'.$imageName;
        }

        $banner = $request->file('banner');
        if($banner) {
            $imageBanner = time().'.'.$banner->getClientOriginalExtension();
            $destinationPathB = public_path(self::PATH_BANNER);
            $banner->move($destinationPathB, $imageBanner);
            $data['banner'] = self::PATH_BANNER.'/'.$imageBanner;
        }

        $positionImg = $request->file('positions');
        if($positionImg) {
            $positionImg = $positionImg['image'];
            $imageNamePosition = time().'.'.$positionImg->getClientOriginalExtension();
            $destinationPathP = public_path(self::PATH_POSITION);
            $positionImg->move($destinationPathP, $imageNamePosition);
            $data['positions']['image'] = self::PATH_POSITION.'/'.$imageNamePosition;
        }

        $dataImages = $this->converDataImages($data, $project, 'images', self::PATH_IMAGES);
        $dataUtilities = $this->converDataImages($data, $project, 'utilities', self::PATH_UTILITY);

        $project = $this->projectService->update($project, $data);

        return redirect()->route('admin.project.index')->withFlashSuccess(__('The project was successfully updated.'));
    }

    public function converDataImages($data, $project, $key, $path) {
        $images = isset($project[$key]['images']) ? $project[$key]['images'] : [];
        $imagesM = $project->getMedia($key);
        $imagesImg = isset($data[$key]['images']) ? $data[$key]['images'] : [];
        $imagesDel = []; $imagesIns = [];
        foreach($imagesM as $item) {
            if(!in_array($item->file_name, $imagesImg)) {
                // Xóa file ảnh
                $dstfileD = public_path($path).'/'.$item->file_name;
                $item->delete();
                if (file_exists($dstfileD)) {
                    unlink($dstfileD);
                }
                array_push($imagesDel, $item);
            }
        }
        foreach($imagesImg as $item) {
            if(!in_array($item, $images)) {
                // Thêm ảnh
                $dstfile = public_path($path).'/'.$item;
                if (!file_exists(dirname($dstfile))) {
                    mkdir(dirname($dstfile), 0777, true);
                }
                $imgT = storage_path('tmp/uploads/' . $item);
                if(file_exists($imgT)) {
                    copy($imgT, $dstfile);
                    $project->addMedia(storage_path('tmp/uploads/' . $item))->toMediaCollection($key);
                    array_push($imagesIns, $item);
                }
            }
        }
        return [
            'del' => $imagesDel,
            'ins' => $imagesIns
        ];
    }

    /**
     * @param  DeleteProjectRequest  $request
     * @param  Project  $project
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteProjectRequest $request, Project $project)
    {
        $this->projectService->destroy($project);

        return redirect()->route('admin.project.index')->withFlashSuccess(__('The project was successfully deleted.'));
    }
}

<?php

namespace App\Http\Requests\Backend\Setting;

use App\Models\Setting;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateSettingRequest.
 */
class UpdateSettingRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'value' => ['max:10000000'],
        ];
    }
}

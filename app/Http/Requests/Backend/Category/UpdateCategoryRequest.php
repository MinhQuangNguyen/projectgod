<?php

namespace App\Http\Requests\Backend\Category;

use App\Models\Category;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateCategoryRequest.
 */
class UpdateCategoryRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:100'],
            'description' => ['max:10000000'],
            'slug' => ['max:255'],
            'type' => [Rule::in([Category::TYPE_NEW, Category::TYPE_PROJECT, Category::TYPE_LIBRARY])],
            'image' => ['max:10000'],
            'sort' => ['nullable', 'numeric'],
            'active' => ['required', 'numeric']
        ];
    }
}

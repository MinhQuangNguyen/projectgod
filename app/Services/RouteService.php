<?php

namespace App\Services;

use App\Models\Route;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class RouteService.
 */
class RouteService extends BaseService
{
    /**
     * ProjectService constructor.
     *
     * @param  Project  $project
     */
    public function __construct(Route $route)
    {
        $this->model = $route;
    }

    public function getByUrl($url)
    {
        $query = $this->model::query();
        
        $query = $query->where('url', "/".$url)
        ->where('active', 1);
        return $query->firstOrFail();
    }

}

<?php

namespace App\Services;

use App\Models\Project;
use App\Models\Post;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Illuminate\Pagination\LengthAwarePaginator;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class SearchService.
 */
class SearchService extends BaseService
{
    /**
     * SearchService constructor.
     *
     * @param  Project  $project
     * @param  Post  $post
     */
    public function __construct(Project $project, Post $post)
    {
        $this->project = $project;
        $this->post = $post;
    }

    public function getProjectByCondition($keyword) {
        $locale = session()->get('locale') ?? env('APP_LANGUAGE');
        $query = $this->project->query()->join('project_translations as pj', function ($query) use ($locale) {
            $query->on('projects.id', '=', 'pj.project_id')
                ->where('pj.locale', '=', $locale);
        })->select('name', 'description', 'category_id', 'slug', 'image');

        if ($keyword) $query = $query->where(function ($builder) use ($keyword) {
            $builder->orWhereTranslationLike('name', "%$keyword%");
            $builder->orWhereTranslationLike('description', 'like', "%$keyword%");
        });
        return $query->get();
    }

    public function getPostByCondition($keyword) {
        $locale = session()->get('locale') ?? env('APP_LANGUAGE');
        $post = $this->post->query()->join('post_translations as t', function ($query) use ($locale) {
            $query->on('posts.id', '=', 't.post_id')
                ->where('t.locale', '=', $locale);
        })->select('t.title as name', 't.description_short as description', 'category_id', 'slug', 'title', 'tagser', 'image', 'author', 'created_at');
        if ($keyword) $post = $post->where(function ($builder) use ($keyword) {
            $builder->orWhereTranslationLike('title', "%$keyword%");
            $builder->orWhereTranslationLike('description_short', 'like', "%$keyword%");
        });
        return $post->get();
    }

}

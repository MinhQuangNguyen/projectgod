<?php

namespace App\Services;

use App\Models\Degree;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class DegreeService.
 */
class DegreeService extends BaseService
{
    /**
     * DegreeService constructor.
     *
     * @param  Degree  $degree
     */
    public function __construct(Degree $degree)
    {
        $this->model = $degree;
    }

    /**
     * @return mixed
     */
    public function getDegrees($id = null)
    {
        $query = $this->model::where('active', 1)->orderBy('sort', 'asc');
        return $query->get();
    }
}

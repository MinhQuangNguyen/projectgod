<?php

namespace App\Services;

use App\Events\Setting\SettingDeleted;
use App\Events\Setting\SettingUpdated;
use App\Models\Setting;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class SettingService.
 */
class SettingService extends BaseService
{
    /**
     * SettingService constructor.
     *
     * @param  Setting  $setting
     */
    public function __construct(Setting $setting)
    {
        $this->model = $setting;
    }

    /**
     * @param  Setting  $setting
     * @param  array  $data
     *
     * @return Setting
     * @throws GeneralException
     * @throws \Throwable
     */
    public function update(Setting $setting, array $data = []): Setting
    {
        DB::beginTransaction();
        try {
            $setting->update($data);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating the setting.'));
        }

        event(new SettingUpdated($setting));

        DB::commit();

        return $setting;
    }

    /**
     * @param  Setting  $setting
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Setting $setting): bool
    {
        if ($this->deleteById($setting->id)) {
            event(new SettingDeleted($setting));

            return true;
        }

        throw new GeneralException(__('There was a problem deleting the setting.'));
    }

    //Lấy setting cho footer
    public function getSettingForHome() {
        $data = $this->model::query()->where('active', 1)->get();
        $result = [];
        foreach($data as $item) {
            $result[$item->key] = $item->value;
        }
        return $result;
    }

    public function getSettingByKey($key) {
        return $this->model::query()->where('key', $key)->where('active', 1)->first();
    }
}

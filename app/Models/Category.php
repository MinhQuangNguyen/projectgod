<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class Category extends Model
{
    use Translatable;

    public const TYPE_NEW = 0;
    public const TYPE_PROJECT = 1;
    public const TYPE_LIBRARY = 2;
    public const TYPE_SLIDE = 3;
    public const ACTIVE = 1;
    public const INACTIVE = 0;
    public const KEYS = [
        [ 'key' => 'homepage-top', 'value' => 'Homepage slide' ],
        [ 'key' => 'homepage-new', 'value' => 'Homepage new slide' ],
        [ 'key' => 'homepage-footer', 'value' => 'Homepage footer slide' ],
        [ 'key' => 'new', 'value' => 'Slide new page' ],
        [ 'key' => 'recruitment', 'value' => 'Slide under the recruitment menu' ],
        [ 'key' => 'recruitment-value', 'value' => 'Slide recruitment under core values' ],
        [ 'key' => 'recruitment-images', 'value' => 'Recruitment Images' ],
        [ 'key' => 'project', 'value' => 'Slide project page' ],
    ];
    public $translatedAttributes = ['name', 'description', 'slug'];

    // protected $fillable = array('*');
    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }

    public function menus()
    {
        return $this->hasMany('App\Models\Menu');
    }

    public function slides()
    {
        return $this->hasMany('App\Models\Slide');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post')->orderBy('sort', 'asc');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }

    public function getNews($limit = 5)
    {
        return $this->posts->where('active', 1)->take($limit);
    }

    public function getProjects($limit = 5)
    {
        return $this->projects->where('is_highlight', 1)->take($limit);
    }

    /**
     * @return string
     */
    public function convertType(): string
    {
        if ($this->type === self::TYPE_NEW) {
            return __('New');
        }

        if ($this->type === self::TYPE_PROJECT) {
            return __('Project');
        }

        if ($this->type === self::TYPE_LIBRARY) {
            return __('Library');
        }

        return 'N/A';
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        $links = [
            self::TYPE_NEW => [
                'vi' => '/tin-tuc',
                'en' => '/news'
            ],
            self::TYPE_PROJECT => [
                'vi' => '/du-an',
                'en' => '/project'
            ],
            self::TYPE_LIBRARY => [
                'vi' => '/tin-tuc',
                'en' => '/news'
            ]
        ];
        return $links[$this->type][app()->getLocale()];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Project extends Model implements HasMedia
{
    use Translatable, HasMediaTrait;
    public $translatedAttributes = ['name', 'address', 'slug', 'description', 'overviews', 'positions', 'utilities', 'images'];

    protected $fillable = ['image', 'category_id', 'is_highlight', 'banner'];
    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return bool
     */
    public function isHighlight(): bool
    {
        return $this->is_highlight;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}

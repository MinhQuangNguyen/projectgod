<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuTranslation extends Model
{
    protected $fillable = ['name', 'description', 'link', 'content'];
    public $timestamps = false;

    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
}

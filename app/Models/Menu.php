<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Menu extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['name', 'link', 'content'];

    protected $fillable = ['parent_id', 'category_id', 'key', 'view', 'sort'];
    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Lấy danh sách children của menu
     */
    public function getChild(){
        return $this->query()->where('parent_id', $this->id)->get();
    }

    /**
     * Kiểm tra menu có submenu hay không
     */
    public function hasChild(){
        return $this->query()->where('parent_id', $this->id)->count();
    }

    /**
     * Kiểm tra xem link của menu có chứa $keyword hay không
     */
    public function linkContains($keyword){
        return strpos($this->link, $keyword) !== false;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    protected $fillable = ['title', 'slug', 'description_short', 'content'];
    public $timestamps = false;

    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
}
